<?php

class Course extends Eloquent {
	public function students(){
		return $this->hasMany('Student');
	}

	public static function field($name, $value){
		return Form::dropdown($name, Course::all()->lists('name', 'id'), $value);
	}
}