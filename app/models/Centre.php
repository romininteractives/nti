<?php
class Centre extends Eloquent
{
	public function students(){
		return $this->hasMany('Student');
	}

	public static function field($name, $value = null){
		return Form::dropdown($name, Centre::all()->lists('name', 'id'), $value);
	}
}