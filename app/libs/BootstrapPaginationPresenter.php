<?php

class BootstrapPaginatorPresenter extends Illuminate\Pagination\Presenter {
    public function getActivePageWrapper($text)
    {
        return '<li class="active"><a href="#">'.$text.'</a></li>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<li class="disabled"><a href="#">'.$text.'</a></li>';
    }

    public function getPageLinkWrapper($url, $page, $rel = null)
    {
        return '<li><a href="'.$url.'">'.$page.'</a></li>';
    }   

    public function getDots(){
        return '<li><span>...</span></li>';
    }
} 