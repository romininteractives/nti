@extends('frontend/layouts/default')

@section('title')
NTI Student Tracking System
@stop

@section('content')
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form enctype="multipart/form-data" class="form-horizontal" method="get" action="{{ route('home.post') }}">
			<div class="panel panel-default">
				<div class="panel-heading">Find Students</div>
				<div class="panel-body">	
					<div class="form-group">
									<label class="col-md-3 control-label" for="centre">Center:</label>
									<div class="col-md-9">	
										{{ Form::dropdown('centre', Centre::all()->lists('name', 'id'), Input::old('centre')) }}
									</div>
					</div>

					<div class="form-group">
									<label class="col-md-3 control-label" for="course">Course:</label>
									<div class="col-md-9">	
									{{ Form::dropdown('course', Course::all()->lists('name', 'id'), Input::old('course')) }}
									</div>
					</div>
					<div class="form-group">
									<label class="col-md-3 control-label" for="district">District:</label>
									<div class="col-md-9">	
									{{ Form::dropdown('district', Centre::all()->lists('district', 'id'), Input::old('district')) }}
									</div>
					</div>

					<div class="form-group">
									<label class="col-md-3 control-label" for="batch_no">Batch No.</label>
									<div class="col-md-9">	
										{{ Form::dropdown('batch_no', Student::all()->lists('batch_no', 'id'),Input::old('batch_no')) }}
									</div>
					</div>

					<div class="form-group">
									<label class="col-md-3 control-label" for="category">Category:</label>
									<div class="col-md-9">	
									{{ Form::casts('category', Input::old('category')) }}									
									</div>
					</div>

					<div class="form-group">
							<label class="col-md-2 control-label" for="first_name">Name</label>
								<div class="col-md-4">	
									<input type="text" name="first_name" class="form-control" id="first_name" value="{{ Input::get('first_name') }}" />
								</div>

								<label class="col-md-2 control-label" for="last_name">Surname</label>
								<div class="col-md-4">	
									<input type="text" name="last_name"  class="form-control" id="last_name" value="{{ Input::get('last_name') }}" />
								</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label" for="gender">Gender:</label>
						<div class="col-md-4">	
							<input type="radio" name="gender" value="0">Male
							<input type="radio" name="gender" value="1">Female
						</div>
					</div>

				</div>		
				
				<div class="panel-footer clearfix">
					<input type="submit" value="Submit" name="submit">
					<input type="reset" value="Cancel" name="cancel">
				</div>
			</div>
		</form>
	</div>
</div>
@stop