@extends('frontend/layouts/default')

@section('title')
NTI Student Tracking System
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				<h4 class="pull-left">Filtered Data</h4>
				<a href="{{ URL::to('/') }}" class="btn btn-info pull-right">Filter</a>
			</div>
				<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<th>District</th>
							<th>Batch No</th>
							<th>Course</th>
							<th>Photo</th>						
							<th>Name</th>
							<th>Surname</th>
							<th>Gender</th>
							<th>Age</th>
							<th>Catogory</th>
							<th>Mobile</th>
							<th>E-Mail</th>
							<th>Address</th>
							<th>job Address</th>
							<th>Salary</th>
						</tr>
					</thead>
					@foreach($msg as $value)			
						<tr>
							<td>{{$value->district}}</td>
							<td>{{$value->batch_no}}</td>
							<td>{{$value->course->name}}</td>
							<td><img src="{{ $value->photo}}"></td>
							<td>{{$value->first_name}}</td>
							<td>{{$value->last_name}}</td>
							<td>{{$value->gender}}</td>
							<td>{{$value->dob->age}}</td>
							<td>{{$value->category}}</td>
							<td>{{$value->mobile_number}}</td>
							<td>{{$value->email}}</td>
							<td>{{$value->address}}</td>
							<td>{{$value->job_company}}</td>
							<td>{{$value->salary}}</td>														
						</tr>
					@endforeach
				</table>
				</div>
				</div>		
				<div class="panel-footer clearfix" style="padding: 0px 24px;">
					<div class="pull-right">
						<nav>
				  			<ul class="pagination">
  								<?php echo with(new BootstrapPaginatorPresenter($msg))->render(); ?>
  							</ul>
						</nav>
					</div>
				</div>
			</div>
	</div>
</div>
@stop
