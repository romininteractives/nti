@extends('backend.layouts.default')

@section('title')
Update Bank Details - {{ $student->name }}
@stop

@section('content')
<div class="row">
		<div class="col-md-2">
		@include('backend.students.student')
	</div>
	<div class="col-md-10">
		<div class="panel panel-info">
				<div class="panel-heading">
					Bank Details
				</div>
			<div class="panel-body">
			{{ Template::form('updateBankDetails', route('students.update.bank_details.submit', $student->id), [
				'groups' => [
					'Personal' => [
						[
							'name' => 'bank_name',
							'label' => 'Bank Number',
						],
						[
							'name' => 'bank_branch',
							'label' => 'Branch Location',
						],
						[
							'name' => 'bank_address',
							'label' => 'Bank Address',
							'field' => Form::textarea('bank_address', Input::old('bank_address', $student->bank_address), ['class' => 'form-control', 'rows' => 3]),
						],
						[
							'name' => 'bank_ifsc',
							'label' => 'Bank IFSC Code',
						],

						[
							'name' => 'account_name',
							'label' => 'Account Name',
						],
						[
							'name' => 'account_number',
							'label' => 'Account Number',
						],
						[
							'name' => 'account_type',
							'label' => 'Account Type',
							'field' => Form::bool('account_type', Input::old('account_type', $student->account_type), 'Saving', 'Current'),
						],
						[
							'name' => 'passbook_received',
							'label' => 'Is Passbook Received?',
							'field' => Form::bool('passbook_received', Input::old('passbook_received', $student->passbook_received)),
						],
					],
				],	
			], $errors, $student)}}
		</div>
	</div>
</div>
</div>
@stop

@section('footer')
<script type="text/javascript">
{{ (new \Nti\Validators\BankDetailsValidator())->jQuery('#updateBankDetails') }}
</script>
@stop