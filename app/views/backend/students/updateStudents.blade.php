@extends('backend.layouts.default')

@section('title')
Update Student
@stop

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-2">
		@include('backend.students.student')
	</div>
	<div class="col-md-10">
		<div class="panel panel-info">
				<div class="panel-heading">
					Update Student Details
				</div>
				<div class="panel-body">
	{{ Template::form('updatestudent', route('students.update.submit', $student->id), [
		'tabs' => ['Personal', 'Acadamic'],
		'groups' => [
			'Personal' => [
				[
					'name' => 'first_name',
					'label' => 'First Name',
				],
				[
					'name' => 'middle_name',
					'label' => 'Middle Name',
				],
				[
					'name' => 'last_name',
					'label' => 'Last Name',
				],
				[
					'name' => 'mother_name',
					'label' => 'Mother\'s Name',
				],
				[
					'name' => 'gender',
					'label' => 'Gender',
					'field' => Form::bool('gender', Input::old('gender', $student->gender), 'Female', 'Male')
				],
				[
					'name' => 'dob',
					'label' => 'Date of Birth',
					'field' => Form::uidatetime('dob', Input::old('dob', $student->dob->format('Y-m-d')))
				],
				[
					'name' => 'email',
					'label' => 'Email Address',
				],
				[
					'name' => 'address',
					'label' => 'Address',
					'field' => Form::textarea('address', Input::old('address', $student->address), ['class' => 'form-control', 'rows' => 4])
				],
				[
					'name' => 'taluka',
					'label' => 'Taluka',
				],
				[
					'name' => 'district',
					'label' => 'District',
				],
				[
					'name' => 'mobile_number',
					'label' => 'Mobile Number',
				],
				[
					'name' => 'phone_no',
					'label' => 'Landline Number',
				],
				[
					'name' => 'category',
					'label' => 'Category',
				],			
				[
					'name' => 'special_need',
					'label' => 'Special Need',
				],			
			],
			'Acadamic' => [
				[
					'name' => 'education',
					'label' => 'Previous Education(qualificiation)',
				],
				[
					'name' => 'roll_number',
					'label' => 'Roll Number',
				],
				[
					'name' => 'centre',
					'label' => 'Centre',
					'field' => Centre::field('centre', Input::old('centre', $student->centre_id))
				],
				[
					'name' => 'course',
					'label' => 'Course',
					'field' => Course::field('course', Input::old('course', $student->course_id))
				],
				[
					'name' => 'batch_no',
					'label' => 'Batch Code',
				],
				[
					'name' => 'joined_at',
					'label' => 'Joining Date',
					'field' => Form::uidatetime('joined_at', Input::old('joined_at', $student->joined_at->format('Y-m-d')))
				],	
			]
		]
	], $errors ,$student)}}
	</div>
</div>
</div>
</div>
@stop

@section('footer')
<script type="text/javascript">
{{ (new \Nti\Validators\StudentCreateValidator())->jQuery('#updatestudent') }}
</script>
@stop