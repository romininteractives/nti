@extends('backend/layouts/default')
@section('content')
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
		<form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ route('students.add') }}">
			<div class="panel panel-default">
				<div class="panel-heading">Add Student</div>
				<div class="panel-body">	
					
					<div class="form-group {{ (($errors->has('roll_number')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="roll_number">Roll Number:</label>
						<div class="col-md-9">	
							<input class="form-control" type="text" name="roll_number" value="{{ Input::old('roll_number') }}">
							{{$errors->first('roll_number','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('biometric_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="biometric_no">Biometric Number:</label>
						<div class="col-md-9">	
							<input class="form-control" type="text" name="biometric_no" value="{{ Input::old('biometric_no') }}">
							{{$errors->first('biometric_no','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('name')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="name">Name:</label>
						<div class="col-md-9">	
							<input class="form-control" type="text" name="name" value="{{ Input::old('name') }}">
						{{$errors->first('name','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('education')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="education">Education:</label>
						<div class="col-md-9">	
							<input class="form-control" type="text" name="education" value="{{ Input::old('education') }}">
						{{$errors->first('education','<span class="help-block">:message</span>') }}
						</div>
					</div>	

					<div class="form-group {{ (($errors->has('gender')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="gender">Gender:</label>
						<div class="col-md-4">	
							<input type="radio" name="gender" value="0" checked="checked">Male
							<input type="radio" name="gender" value="1">Female
						</div>
					</div>
										
					<div class="form-group {{ (($errors->has('category')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="category">Category:</label>
						<div class="col-md-9">	
						{{ Form::casts('category', Input::old('category')) }}
						{{$errors->first('category','<span class="help-block">:message</span>') }}
						</div>
					</div>	

					<div class="form-group {{ (($errors->has('course')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="course">Course:</label>
						<div class="col-md-9">	
						{{ Form::dropdown('course', Course::all()->lists('name', 'id'), Input::old('course')) }}
						{{$errors->first('course','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('centre')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="centre">Center:</label>
						<div class="col-md-9">	
							{{ Form::dropdown('centre', Centres::all()->lists('name', 'id'), Input::old('centre')) }}
						{{$errors->first('centre','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('doj')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="doj">Date of Join:</label>
						<div class="col-md-9">	
							<input class="form-control" type="text" name="doj" id="datepicker" value="{{ Input::old('doj') }}">							
						{{$errors->first('doj','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('photo')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="doj">Photo:</label>
						<div class="col-md-9">	
							{{ Form::file('photo') }}
							{{$errors->first('photo','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('address')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="address">Address:</label>
						<div class="col-md-9">	
						<textarea class="form-control" type="text" name="address">{{ Input::old('address') }}</textarea>
						{{$errors->first('address','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('dob')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="dob">Date of Birth:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="dob" id="dob" value="{{ Input::old('dob') }}">							
						{{$errors->first('dob','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('batch_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="batch_no">Batch Number:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="batch_no" value="{{ Input::old('batch_no') }}">
						{{$errors->first('batch_no','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('email')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="email">Email:</label>
						<div class="col-md-9">	
						<input class="form-control" type="email" name="email" value="{{ Input::old('email') }}">
						{{$errors->first('email','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ (($errors->has('phone_no')) ? 'has-error' : '') }}">
						<label class="col-md-3 control-label" for="phone_no">Phone Number:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="phone_no" value="{{ Input::old('phone_no') }}">
						{{$errors->first('phone_no','<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label" for="remark">Remarks:</label>
						<div class="col-md-9">	
							<textarea class="form-control" name="remark"></textarea>
						</div>
					</div>
				</div>
				<div class="panel-footer clearfix">
					<input type="submit" value="Submit" name="submit">
					<input type="submit" value="Cancel" name="cancel">
				</div>
			</div>
		</form>
@stop


@section('footer')
	<script>
	  $(function() {
	    $( "#datepicker" ).datepicker();
	    $( "#dob" ).datepicker();
	  });
	</script>
@stop