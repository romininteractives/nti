@extends('backend/layouts/default')

@section('content')
<br>
<a href="{{ URL::to('admin/students/add') }}" type='submit' class="btn btn-success pull-right col-md-1">Add New</a>			
<br>
<br>
<div class="row">
	<div class="col-md-12">
		<form method="post" action="{{ URL::to('admin/students') }}">
			<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
				<tr>
					<th>Delete</th>
					<th>Photo</th>
					<th>Roll #</th>
					<th><center>Name</center></th>
					<th>Centre</th>
					<th>Course</th>
					<th>Batch #</th>
					<th class="col-md-1" align="right">Update</th>
				</tr>
				</thead>
			@foreach($msg as $value)			
				<tr>
					<td><input type='checkbox' name='delete[]' value="{{ $value->id }}"></td>
					<td><img src="{{ $value->photo }}" class="img-thumbnail" width="70" height="70"></img></td>
					<td><center>{{ $value->roll_number }}</center></td>
					<td><a href="{{ route('students.view', $value->id) }}">{{ $value->name}}</a></td>
					<td>{{ $value->centre->name }}</td>
					<td>{{ $value->course->name }}</td>
					<td>{{ $value->batch_no }}</td>
					<td>
						<div class="btn-group">
						  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						    Edit <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
						    <li><a href="{{ route('students.update', $value->id) }}">Personal Info</a></li>
						    <li class="divider"></li>
						    <li><a href="{{ route('students.update.idproof', $value->id) }}">ID Proofs</a></li>
						    <li><a href="{{ route('students.update.self_group', $value->id) }}">Self Help Groups</a></li>
						    <li><a href="{{ route('students.update.bank_details', $value->id) }}">Bank Details</a></li>
						  </ul>
						</div>
					</td></tr>
			@endforeach
			</table>
		</div>
			<button type='submit' class="btn btn-danger pull-left">Delete</button>
		</form>
	</div>
</div>

<div class="row">
	<div class="col-md-offset-10 col-md-2">
		<nav>
  			<ul class="pagination">
  				<?php echo with(new BootstrapPaginatorPresenter($msg))->render(); ?>
  			</ul>
		</nav>
	</div>
</div>
@stop