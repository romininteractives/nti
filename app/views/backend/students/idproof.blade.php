@extends('backend.layouts.default')

@section('title')
Update Id Proofs {{ $student->name }}
@stop


@section('content')
<div class="row">
	<div class="col-md-2">
		@include('backend.students.student')
	</div>
	<div class="col-md-10">
			<div class="panel panel-info">
				<div class="panel-heading">
					ID Proofs
				</div>
				<div class="panel-body">
				{{ Template::form('addidproof', route('students.update.idproofs.submit', $student->id), [
					'groups' => [	
						'idproof' => [
							[
								'name' => 'epic_no',
								'label' => 'Epic No',
							],
							[
								'name' => 'uid',
								'label' => 'UID - Aadhar Card',
							],
							[
								'name' => 'ration_card_number',
								'label' => 'Ration Card',
							],
							[
								'name' => 'bpl_proof',
								'label' => 'BPL Proof - optional *',
							],
							[
								'name' => 'driving_license_number',
								'label' => 'Driving License - optional *',
							],
						],
					]
				], $errors, $student)}}
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
<script type="text/javascript">
{{ (new \Nti\Validators\idProofsValidator())->jQuery('#addidproof') }}
</script>
@stop