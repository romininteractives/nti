@extends('backend.layouts.default')

@section('title')
Update Id Proofs {{ $student->name }}
@stop


@section('content')
<div class="row">
	<div class="col-md-2">
		@include('backend.students.student')
	</div>
	<div class="col-md-10">
			<div class="panel panel-info">
				<div class="panel-heading">
					Self Help group
				</div>
				<div class="panel-body">
					{{ Template::form('updateselfgroup', route('students.update.self_group.submit', $student->id), [
						'groups' => [	
							'selfgroup' => [
								[
									'name' => 'shg_group',
									'label' => 'Group Name',
								],
								[
									'name' => 'shg_group_member',
									'label' => 'Members Name',
								],
								[
									'name' => 'shg_district',
									'label' => 'District',
								],
								[
									'name' => 'shg_taluka',
									'label' => 'Taluka',
								],
								[
									'name' => 'shg_village',
									'label' => 'Village',
								],
							],
						]
					], $errors, $student)}}
			</div>
		</div>
	</div>
</div>
@stop


@section('footer')
<script type="text/javascript">
{{ (new \Nti\Validators\SelfHelpGroupValidator())->jQuery('#updateselfgroup') }}
</script>
@stop