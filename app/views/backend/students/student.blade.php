 <div class="panel panel-success" style="padding: 30px 10px;">
			<ul class="nav nav-pills nav-stacked">
			<li{{ Request::is('admin/students/update/*') ? ' class="active"' : '' }}>
				<a href="{{ route('students.update', $student->id) }}">Personal Info</a>
			</li>	
			<li{{ Request::is('admin/students/*/update/idproof') ? ' class="active"' : '' }}>
				<a href="{{ route('students.update.idproof', $student->id) }}">ID Proofs</a>
			</li>
			
			<li{{ Request::is('admin/students/*/update/bank-details') ? ' class="active"' : '' }}>
				<a href="{{ route('students.update.bank_details', $student->id) }}">Bank Details</a>
			</li>

			<li{{ Request::is('admin/students/*/update/self_group') ? ' class="active"' : '' }}>
				<a href="{{ route('students.update.self_group', $student->id) }}">Self Help Groups</a>
			</li>
		</ul>
</div>