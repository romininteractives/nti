@extends('backend.layouts.default')

@section('title')
Enroll New Student
@stop

@section('content')
<br>
<br>
<div class="row">
	<div class="col-md-offset-3 col-md-6">
	{{ Template::form('addStudent', route('students.add.submit'), [
		'tabs' => ['Personal', 'Acadamic'],
		'groups' => [
			'Personal' => [
				[
					'name' => 'first_name',
					'label' => 'First Name',
				],
				[
					'name' => 'middle_name',
					'label' => 'Middle Name',
				],
				[
					'name' => 'last_name',
					'label' => 'Last Name',
				],
				[
					'name' => 'mother_name',
					'label' => 'Mother\'s Name',
				],
				[
					'name' => 'gender',
					'label' => 'Gender',
					'field' => Form::bool('gender', Input::old('gender'), 'Female', 'Male')
				],
				[
					'name' => 'dob',
					'label' => 'Date of Birth',
					'field' => Form::uidatetime('dob', Input::old('dob'))
				],
				[
					'name' => 'email',
					'label' => 'Email Address',
				],
				[
					'name' => 'address',
					'label' => 'Address',
					'field' => Form::textarea('address', Input::old('address'), ['class' => 'form-control', 'rows' => 4])
				],
				[
					'name' => 'taluka',
					'label' => 'Taluka',
				],
				[
					'name' => 'district',
					'label' => 'District',
				],
				[
					'name' => 'mobile_number',
					'label' => 'Mobile Number',
				],
				[
					'name' => 'phone_no',
					'label' => 'Landline Number',
				],
				[
					'name' => 'category',
					'label' => 'Category',
					'field' => Form::dropdown('category',['ST' => 'ST' ,'SC' => 'SC' ,'OBC' => 'OBC' ,'General' => 'General'],  Input::old('category')),
				],			
				[
					'name' => 'special_need',
					'label' => 'Special Need',
				],			
			],
			'Acadamic' => [
				[
					'name' => 'education',
					'label' => 'Previous Education(qualificiation)',
				],
				[
					'name' => 'roll_number',
					'label' => 'Roll Number',
				],
				[
					'name' => 'centre',
					'label' => 'Centre',
					'field' => Centre::field('centre', Input::old('centre'))
				],
				[
					'name' => 'course',
					'label' => 'Course',
					'field' => Course::field('course', Input::old('course'))
				],
				[
					'name' => 'batch_no',
					'label' => 'Batch Code',
				],
				[
					'name' => 'joined_at',
					'label' => 'Joining Date',
					'field' => Form::uidatetime('joined_at', Input::old('joined_at'))
				],	
			]
		]
	], $errors)}}
	</div>
</div>
@stop

@section('footer')
<script type="text/javascript">
{{ (new \Nti\Validators\StudentCreateValidator())->jQuery('#addStudent') }}
</script>
@stop