@extends('backend/layouts/default')
@section('title')
Centre Detail
@parent
@stop


@section('content')
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
		<form class="form-horizontal" action="{{ route('create.centres') }}" method="post">
			<div class="panel panel-default">
				<div class="panel-heading text-center">Enter the Centre Detail</div>
				<div class="panel-body">	
	
				<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label class="col-md-3 control-label" for="name">Name:</label>
							<div class="col-md-9">	
								<input class="form-control" type="text" name="name" id="name" value="{{ Input::old('name') }}" >
								{{ $errors->first('name', '<span class="help-block">:message</span>') }}
							</div>
				</div>
			
					<div class="form-group {{ $errors->has('place') ? 'has-error' : '' }}">
						<label class="col-md-3 control-label" for="place">Place:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="place" id="place" value="{{ Input::old('place') }}" >
								{{ $errors->first('place', '<span class="help-block">:message</span>') }}
						</div>
					</div>
			
					<div class="form-group {{ $errors->has('district') ? 'has-error' : '' }}" >
						<label class="col-md-3 control-label" for="district">District:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="district" id="district" value="{{ Input::old('district') }}" >
								{{ $errors->first('district', '<span class="help-block">:message</span>') }}
						</div>
					</div>

					<div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
						<label class="col-md-3 control-label" for="phone_no">Phone No:</label>
						<div class="col-md-9">	
						<input class="form-control" type="text" name="phone_no" id="phone_no" value="{{ Input::old('phone_no') }}" >
								{{ $errors->first('phone_no', '<span class="help-block">:message</span>') }}
						</div>
					</div>
					
					<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
						<label class="col-md-3 control-label" for="address">Address</label>
						<div class="col-md-9">	
						<textarea class="form-control" name="address" id="address" value="{{ Input::old('address') }}" >
								</textarea>{{ $errors->first('address', '<span class="help-block">:message</span>') }}
						</div>
					</div>
				</div>
					<div class="panel-footer text-center clearfix">
						<input type="submit" class="btn btn-success">
						<input type="button" value="Cancel" class="btn btn-danger">
					</div>
				</div>
			</form>
@stop