<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Basic Page Needs
		================================================== -->
		<meta charset="utf-8" />
		<title>
			@section('title')
			Administration
			@show
		</title>
		<meta name="author" content="Romin Interactive" />

		<!-- Mobile Specific Metas
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- CSS
		================================================== -->
		<!--<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/bootstrap-responsive.css') }}" rel="stylesheet">-->


		<link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/bootstrap/css/bootstrap-theme.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/ui.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">



		<style>
		@section('styles')
		body {
			padding: 70px 0;
		}
		@show
		</style>

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Favicons
		================================================== -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
		<link rel="apple-touch-icon-precomposed" href="{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">
		<link rel="shortcut icon" href="{{ asset('assets/ico/favicon.png') }}">

		{{ Template::renderCSS() }}
		{{ Template::renderRawCSS() }}
	</head>

	<body>
		<!-- Container -->
		<div class="container">
			<!-- Navbar -->
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="{{ route('admin') }}">NTI.cp</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      
		      		<ul class="nav navbar-nav">
						<li{{ (Request::is('admin/centres*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/centres') }}"><i class="icon-home icon-white"></i> Centres</a></li>
						<li{{ (Request::is('admin/companies*') ? ' class="active"' : '') }}><a href="{{ route('companies') }}"><i class="icon-home icon-white"></i> Placement Companies</a></li>
						<li{{ (Request::is('admin/students*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/students') }}"><i class="icon-home icon-white"></i> Students</a></li>
						<!-- <li class="dropdown{{ (Request::is('admin/users*|admin/groups*') ? ' active' : '') }}">
							<a class="dropdown-toggle" data-toggle="dropdown" href="{{ URL::to('admin/users') }}">
								<i class="icon-user icon-white"></i> Staff <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/users') }}"><i class="icon-user"></i> Members</a></li>
								<li{{ (Request::is('admin/groups*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/groups') }}"><i class="icon-user"></i> Groups</a></li>
							</ul>
						</li> -->
					</ul>
					<form class="navbar-form navbar-left" role="search" action="{{ url('admin/students') }}">
			        <div class="form-group">
			          <input type="text"  required="required" class="form-control" name="q" placeholder="Enter Name/Roll Number">
			        </div>
			        <button type="submit" class="btn btn-info">Search</button>
			     </form>
			      
					<ul class="nav navbar-nav navbar-right">
						<li><a href="{{ URL::to('/') }}">Tracking System</a></li>
						<li class="divider-vertical"></li>
						<li><a href="{{ route('logout') }}">Logout</a></li>
					</ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>

			<!-- Notifications -->
			@include('frontend/notifications')

			<!-- Content -->
			@yield('content')
		</div>

		<script type="text/javascript" src="{{ asset('assets/js/jquery.1.10.2.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>		
		<script type="text/javascript" src="{{ asset('assets/js/jquery-ui.js') }}"></script>

		<script type="text/javascript" src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

		{{ Template::renderJS() }}
		{{ Template::renderRawJS() }}
		@yield('footer')
	</body>
</html>
