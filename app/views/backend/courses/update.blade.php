@extends('backend/layouts/default')

@section('title')
Editing {{ $course->name }}
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Editing {{ $course->name }}</h4>
			</div>
			<div class="panel-body">
				{{ Template::form('add_course_form', route('courses.update', $course->id), array(
					'tabs' => array('General'),
					'tabsContent' => array(
						'General' => array(
							array(
								'name' => 'name',
								'label' => 'Name',
							),
							array(
								'name' => 'code',
								'label' => 'Code',
								'hint' => 'Must be unique'
							),
							array(
								'name' => 'description',
								'label' => 'Description',
								'field' => Form::textarea('description', Input::old('description', $course->description), array('class' => 'form-control')),
							),
						)
					)
				), $errors, $course) }}
			</div>
		</div>
	</div>
</div>
@stop