<?php

Form::macro('bool', function($name, $value, $yesValue = 'Yes', $noValue = 'No'){
	$html = '<div class="radio"><input type="radio" name="'.$name.'" value="1"'.(($value === 1 || $value == $yesValue) ? ' checked' : '').'/> '.$yesValue.'&nbsp;&nbsp;<br/>';
	$html .= '<input type="radio" name="'.$name.'" value="0"'.(($value === 0 || $value == $noValue) ? ' checked' : '').'/> '.$noValue.'</div>';

	return $html;
});

Form::macro('dropdown', function($name, $options = array(), $value = null, $multiSelect = false, $attributes = array()){
	$values = array();
	if(is_array($value))
	{
		foreach ($value as $val) {
			$values[] = $val;
		}
	}

	$attributesStr = null;
	if(is_array($attributes) && count($attributes) > 0){
		foreach($attributes as $key => $value){
			$attributesStr .= $key.'="'.$value.'" ';
		}
	}
	
	if($multiSelect == true)
	{
		Template::addRawJS('
			$(document).ready(function(){
				$("#'.$name.'").select2();
			});
		');
	}
	
	$html = '<select'.(($multiSelect == true) ? ' multiple' : '').' class="form-control" name="'.$name.(($multiSelect == true) ? '[]' : '').'" id="'.$name.'" '.$attributesStr.'>';
	
	if($multiSelect === false)
	{
		$html .= '<option value="">-- Select --</option>';
	}

	foreach($options as $key => $label)
	{
		$html .= '<option '.(
			(
				($multiSelect == false) 
				? ($key == $value || $label == $value) 
				: (in_array($key, $values) || in_array($label, $values))
			) 
			? 'selected ' 
			: '').'value="'.$key.'">'.$label.'</option>';
	}

	$html .= '</select>';

	return $html;
});

Form::macro('tags', function($name, $value = null, $attributes = array()){
	Template::addJS(asset('assets/js/libs/jquery.tagsinput.js'));
	Template::addRawJS('
		$("#'.$name.'").tagsInput({
		height: 40
		});
	');
	if(! isset($attributes['id']))
	{
		$attributes['id'] = $name;
	}
	return Form::text($name, $value, $attributes);
});

Form::macro('uploader', function($name, $action){
	Template::addJS(asset('assets/plugins/files/plupload/plupload.js'));
	Template::addJS(asset('assets/plugins/files/plupload/plupload.html5.js'));
	Template::addJS(asset('assets/plugins/files/plupload/jquery.plupload.queue/jquery.plupload.queue.js'));
	Template::addRawJS("
		// Setup html5 version
    	$(\"#".$name."\").pluploadQueue({
	        // General settings
	        runtimes : 'html5,flash,silverlight,html4',
	        url : \"".$action."\",
	         
	        chunk_size : '1mb',
	        rename : true,
	        dragdrop: true,
	        unique_names: true,
	         
	        filters : {
	            // Maximum file size
	            max_file_size : '1mb',
	            // Specify what files to browse for
	            mime_types: [
	                {title : \"Image files\", extensions : \"jpg,gif,png\"}
	            ]
	        },
	 
	        // Resize images on clientside if we can
	        resize: {
	            quality : 70
	        },
	 
	 
	        // Flash settings
	        flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',
	 
	        // Silverlight settings
	        silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',

	        init: {
	        	FileUploaded: function(up, file, info) {
	        		info = JSON.parse(info.response);
	        		console.log(info);
		        	$('#filesList').val($('#filesList').val() + '<input type=\"hidden\" name=\"".$name."[]\" value=\"'+ info.result +'\" />');
		        }
	        }
	    });
	");
	Template::addCSS(asset('assets/plugins/files/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css'));

	$html = '<div id="'.$name.'" style="width: 100%; height: 100%; margin-bottom: 20px;">File uploading is not supported in this browser.</div>
	<div id="filesList"></div>';

	return $html;
});

Form::macro('datetime', function($name, $value, $dataTemplate, $viewTemplate = null){
	$html = Form::text($name, $value, array(
		'id' => $name,
		'data-format' => $dataTemplate,
		'data-template' => (($viewTemplate == null) ? $dataTemplate : $viewTemplate),
		//'data-value' => $value,
	));

	Template::addRawJS('
		$(function(){
		    $("#'.$name.'").combodate();    
		    //$("#'.$name.'").setValue("'.$value.'");
		});
	');

	return $html;
});

Form::macro('uidatetime', function($name, $value, $includeTime = false, $attributes = array()){
	$attributesStr = null;
	if(is_array($attributes) && count($attributes) > 0){
		foreach($attributes as $key => $value){
			$attributesStr .= $key.'="'.$value.'" ';
		}
	}

	$html = '<input type="text" name="'.$name.'" value="'.$value.'" id="'.$name.'" class="form-control" '.$attributesStr.'/>';

	Template::addRawJS('
		$("#'.$name.'").date'.(($includeTime == true) ? 'time' : '').'picker({
			dateFormat: "yy-mm-dd",
			timeFormat: "hh:mm:ss",
			changeMonth: true,
		    changeYear: true,
      		firstDay: 1,
      		defaultDate: "'.$value.'"
		});
	');
	if($includeTime == true)
	{
		Template::addJS(asset('assets/js/supr-theme/jquery-ui-timepicker-addon.js'));
	}

	return $html;
});

Form::macro('year', function($name, $value = null, $start_offset = 3, $end_offset = 0, $multiple = false){
	$html = '<select class="form-control" name="'.$name.(($multiple == true) ? '[]' : '').'" '.(($multiple == true) ? 'multiple="multiple"' : '').' id="'.$name.'">';
	$current_year = date('Y', time());

	for($i = $current_year-$start_offset; $i <= $current_year+$end_offset; $i++){
		$html .= '<option '.(
				($i == $value) 
				? 'selected ' 
				: ''
			).'value="'.$i.'" '.(($i == $current_year) ? 'selected="selected"' : '').'>'.$i.'</option>';
	}

	$html .= '</select>';

	return $html;
});

Form::macro('casts', function($name, $value, $multi = false, $atts = array()){
	$options = array(
		'General' => 'General',
		'SC' => 'SC',
		'ST' => 'ST',
		'OBC' => 'OBC',
	);

	return Form::dropdown($name, $options, $value, $multi, $atts);
});