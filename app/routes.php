<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Register all the admin routes.
|
*/

Route::group(array('prefix' => 'admin'), function()
{

	# Course management
	Route::group(array('prefix' => 'courses'), function(){
		Route::get('/', array('as' => 'courses', 'uses' => 'Controllers\Admin\Courses\CoursesController@getIndex'));

		Route::get('/create', array('as' => 'courses.create', 'uses' => 'Controllers\Admin\Courses\CoursesController@getCreate'));
		Route::post('/create', array('as' => 'courses.create', 'uses' => 'Controllers\Admin\Courses\CoursesController@postCreate'));

		Route::get('/{id}/update', array('as' => 'courses.update', 'uses' => 'Controllers\Admin\Courses\CoursesController@getUpdate'));
		Route::post('/{id}/update', array('as' => 'courses.update', 'uses' => 'Controllers\Admin\Courses\CoursesController@postUpdate'));

		Route::get('/{id}/delete', array('as' => 'courses.delete', 'uses' => 'Controllers\Admin\Courses\CoursesController@getDelete'));
	});

	# Blog Management
	Route::group(array('prefix' => 'blogs'), function()
	{
		Route::get('/', array('as' => 'blogs', 'uses' => 'Controllers\Admin\BlogsController@getIndex'));
		Route::get('create', array('as' => 'create/blog', 'uses' => 'Controllers\Admin\BlogsController@getCreate'));
		Route::post('create', 'Controllers\Admin\BlogsController@postCreate');
		Route::get('{blogId}/edit', array('as' => 'update/blog', 'uses' => 'Controllers\Admin\BlogsController@getEdit'));
		Route::post('{blogId}/edit', 'Controllers\Admin\BlogsController@postEdit');
		Route::get('{blogId}/delete', array('as' => 'delete/blog', 'uses' => 'Controllers\Admin\BlogsController@getDelete'));
		Route::get('{blogId}/restore', array('as' => 'restore/blog', 'uses' => 'Controllers\Admin\BlogsController@getRestore'));
	});

	# User Management
	Route::group(array('prefix' => 'users'), function()
	{
		Route::get('/', array('as' => 'users', 'uses' => 'Controllers\Admin\UsersController@getIndex'));
		Route::get('create', array('as' => 'create/user', 'uses' => 'Controllers\Admin\UsersController@getCreate'));
		Route::post('create', 'Controllers\Admin\UsersController@postCreate');
		Route::get('{userId}/edit', array('as' => 'update/user', 'uses' => 'Controllers\Admin\UsersController@getEdit'));
		Route::post('{userId}/edit', 'Controllers\Admin\UsersController@postEdit');
		Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'Controllers\Admin\UsersController@getDelete'));
		Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'Controllers\Admin\UsersController@getRestore'));
	});

	# Group Management
	Route::group(array('prefix' => 'groups'), function()
	{
		Route::get('/', array('as' => 'groups', 'uses' => 'Controllers\Admin\GroupsController@getIndex'));
		Route::get('create', array('as' => 'create/group', 'uses' => 'Controllers\Admin\GroupsController@getCreate'));
		Route::post('create', 'Controllers\Admin\GroupsController@postCreate');
		Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'Controllers\Admin\GroupsController@getEdit'));
		Route::post('{groupId}/edit', 'Controllers\Admin\GroupsController@postEdit');
		Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'Controllers\Admin\GroupsController@getDelete'));
		Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'Controllers\Admin\GroupsController@getRestore'));
	});

	# Dashboard
	Route::get('/', array('as' => 'admin', 'uses' => 'Controllers\Admin\DashboardController@getIndex'));


	Route::group(array('prefix' => 'centres'), function()
	{
		Route::get('/', array('as' => 'centres', 'uses' => 'Controllers\Admin\Centres\CentresController@getView'));

		Route::get('create', array('as' => 'create.centres', 'uses' => 'Controllers\Admin\Centres\CentresController@getCreate'));
		Route::post('create','Controllers\Admin\Centres\CentresController@postCreate');
		Route::get('update/{id}', array('as' => 'update.centres', 'uses' => 'Controllers\Admin\Centres\CentresController@getUpdate'));
		Route::post('update/{id}','Controllers\Admin\Centres\CentresController@postUpdate');
		Route::post('delete', array('as' => 'delete.centres', 'uses' => 'Controllers\Admin\Centres\CentresController@postDelete'));
	});

	Route::group(array('prefix' => 'companies'), function()
	{
		Route::get('/', array('as' => 'companies', 'uses' => 'Controllers\Admin\Companies\CompanyController@getView'));

		Route::get('create', array('as' => 'create.company', 'uses' => 'Controllers\Admin\Companies\CompanyController@getCreate'));
		Route::post('create','Controllers\Admin\Companies\CompanyController@postCreate');
		Route::get('update/{id}', array('as' => 'update.company', 'uses' => 'Controllers\Admin\Companies\CompanyController@getUpdate'));
		Route::post('update/{id}','Controllers\Admin\Companies\CompanyController@postUpdate');
		Route::post('delete', array('as' => 'delete.company', 'uses' => 'Controllers\Admin\Companies\CompanyController@postDelete'));
	});

		
	# Students Route Start-------------------------------

	Route::group(array('prefix' => 'students'), function()
	{
		Route::get('/', array('as' => 'students', 'uses' => 'Controllers\Admin\Students\StudentsController@getView'));
		Route::post('/', array('as' => 'students.delete', 'uses' => 'Controllers\Admin\Students\StudentsController@postDelete'));	
		
		Route::get('/{id}/show', array('as' => 'students.view', 'uses' => 'Controllers\Admin\Students\StudentsController@getSingle'));

		Route::get('/add', array('as' => 'students.add', 'uses' => 'Controllers\Admin\Students\CreateStudentController@viewForm'));
		Route::post('/add', array('as' => 'students.add.submit', 'uses' => 'Controllers\Admin\Students\CreateStudentController@addStudent'));

		// update student Details
		Route::get('{studentId}/update', array('as' => 'students.update', 'uses' => 'Controllers\Admin\Students\CreateStudentController@getUpdate'));
		Route::post('{studentId}/update', array('as' => 'students.update.submit', 'uses' => 'Controllers\Admin\Students\CreateStudentController@postUpdate'));

		// id Prof routes
		
		Route::get('{studentId}/update/idproof', array('as' => 'students.update.idproof', 'uses' => 'Controllers\Admin\Students\IdProofsController@viewForm'));
		Route::post('{studentId}/update/idproof', array('as' => 'students.update.idproofs.submit', 'uses' => 'Controllers\Admin\Students\IdProofsController@update'));

		// Bank Details
		Route::get('{studentId}/update/bank-details', array('as' => 'students.update.bank_details', 'uses' => 'Controllers\Admin\Students\UpdateBankDetailsController@viewForm'));
		Route::post('{studentId}/update/bank-details', array('as' => 'students.update.bank_details.submit', 'uses' => 'Controllers\Admin\Students\UpdateBankDetailsController@update'));

		// self help groups
		Route::get('{studentId}/update/self_group', array('as' => 'students.update.self_group', 'uses' => 'Controllers\Admin\Students\SelfHelpGroupController@viewForm'));
		Route::post('{studentId}/update/self_group', array('as' => 'students.update.self_group.submit', 'uses' => 'Controllers\Admin\Students\SelfHelpGroupController@update'));

		Route::get('/{id}/track', array('as' => 'students.track', 'uses' => 'Controllers\Admin\Students\StudentsController@getTrack'));
		Route::post('/{id}/track', array('as' => 'students.track', 'uses' => 'Controllers\Admin\Students\StudentsController@postTrack'));

		Route::get('/update/{id}', array('as' => 'students.update', 'uses' => 'Controllers\Admin\Students\StudentsController@getUpdate'));
		Route::post('/update/{id}', array('as' => 'students.update', 'uses' => 'Controllers\Admin\Students\StudentsController@postUpdate'));
	});
	
	#Students Route End-----------------------------------

});

/*
|--------------------------------------------------------------------------
| Authentication and Authorization Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'auth'), function()
{

	# Login
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');

	# Register
	Route::get('signup', array('as' => 'signup', 'uses' => 'AuthController@getSignup'));
	Route::post('signup', 'AuthController@postSignup');

	# Account Activation
	Route::get('activate/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));

	# Forgot Password
	Route::get('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@getForgotPassword'));
	Route::post('forgot-password', 'AuthController@postForgotPassword');

	# Forgot Password Confirmation
	Route::get('forgot-password/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

});

/*
|--------------------------------------------------------------------------
| Account Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'account'), function()
{

	# Account Dashboard
	Route::get('/', array('as' => 'account', 'uses' => 'Controllers\Account\DashboardController@getIndex'));

	# Profile
	Route::get('profile', array('as' => 'profile', 'uses' => 'Controllers\Account\ProfileController@getIndex'));
	Route::post('profile', 'Controllers\Account\ProfileController@postIndex');

	# Change Password
	Route::get('change-password', array('as' => 'change-password', 'uses' => 'Controllers\Account\ChangePasswordController@getIndex'));
	Route::post('change-password', 'Controllers\Account\ChangePasswordController@postIndex');

	# Change Email
	Route::get('change-email', array('as' => 'change-email', 'uses' => 'Controllers\Account\ChangeEmailController@getIndex'));
	Route::post('change-email', 'Controllers\Account\ChangeEmailController@postIndex');

});




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('about-us', function()
{
	//
	return View::make('frontend/about-us');
});

Route::get('contact-us', array('as' => 'contact-us', 'uses' => 'ContactUsController@getIndex'));
Route::post('contact-us', 'ContactUsController@postIndex');

Route::get('blog/{postSlug}', array('as' => 'view-post', 'uses' => 'BlogController@getView'));
Route::post('blog/{postSlug}', 'BlogController@postView');

//Route::get('/', array('as' => 'home', 'uses' => 'HomeController@showWelcome'));
Route::get('/', array('as' => 'home', 'uses' => 'TestController@getFilter'));
Route::get('/show', array('as' => 'home.post', 'uses' => 'TestController@postFilter'));
Route::get('view/{id}',array('as'=>'home.view','uses'=>'TestController@getSingleView'));

