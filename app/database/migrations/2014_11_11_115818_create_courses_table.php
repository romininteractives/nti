<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('courses', function(Blueprint $table){
			$table->increments('id');

			$table->string('code')->unique();
			$table->string('name');
			$table->text('description')->nullable();

			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('courses');
	}

}
