<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfessonalFieldsToStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('students', function($table){
			$table->string('designation')->nullable();
			$table->decimal('salary', 10, 2)->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('students', function($table){
			$table->dropColumn('designation');
			$table->dropColumn('salary');
		});
	}

}
