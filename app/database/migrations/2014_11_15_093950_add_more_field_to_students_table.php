<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldToStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('students', function(Blueprint $table){
			$table->string('email')->nullable();
			$table->string('phone_no')->nullable();
			$table->string('photo')->nullable();
			$table->text('address')->nullable();
			$table->date('dob');
			$table->string('batch_no')->nullable();
			$table->integer('company_id')->unsigned()->nullable(); // Virtual foreign key
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('students', function(Blueprint $table){
			$table->dropColumn('email');
			$table->dropColumn('phone_no');
			$table->dropColumn('photo');
			$table->dropColumn('address');
			$table->dropColumn('dob');
			$table->dropColumn('batch_no');
			$table->dropColumn('company_id');
		});
	}

}
