<?php

use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `Comments` table
		Schema::create('comments', function($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('parent_id')->unsigned()->nullable();
			$table->integer('post_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->text('content');
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `Comments` table
		Schema::drop('comments');
	}

}
