<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToStudents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('students', function(Blueprint $table)
		{
			// Trainee from rural area
			$table->string('shg_group')->nullable();
			$table->string('shg_group_member')->nullable();
			$table->string('shg_district')->nullable();
			$table->string('shg_taluka')->nullable();
			$table->string('shg_village')->nullable();

			$table->string('epic_no')->nullable();
			$table->string('mother_name')->nullable();
			$table->string('special_need')->nullable();
			$table->string('uid')->nullable();
			$table->string('ration_card_number')->nullable();
			$table->string('bpl_proof')->nullable();
			$table->string('driving_license_number')->nullable();
			$table->string('trainee_contact_details')->nullable();
			$table->string('district')->nullable();
			$table->string('taluka')->nullable();

			// Bank Account
			$table->string('account_name')->nullable();
			$table->string('account_number')->nullable();			
			$table->string('account_type')->nullable();
			$table->string('bank_name')->nullable();
			$table->string('bank_branch')->nullable();
			$table->string('bank_address')->nullable();
			$table->string('bank_ifsc')->nullable();
			$table->string('passbook_received')->nullable();
			
			// On Job Training Details
			$table->date('ojt_start')->nullable();
			$table->date('ojt_end')->nullable();
			$table->string('ojt_place')->nullable();
			$table->string('ojt_place_address')->nullable();
			$table->string('ojt_person_name')->nullable();
			$table->string('ojt_contact_number')->nullable();
			$table->string('ojt_staypend')->nullable();

			// Placement Details
			$table->date('job_start_date')->nullable();
			$table->string('job_company')->nullable();
			$table->string('job_contact_person')->nullable();
			$table->string('job_contact_person_number')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('students', function(Blueprint $table)
		{
			// Trainee from rural area
			$table->dropColumn('shg_group');
			$table->dropColumn('shg_group_member');
			$table->dropColumn('shg_district');
			$table->dropColumn('shg_taluka');
			$table->dropColumn('shg_village');

			$table->dropColumn('epic_no');
			$table->dropColumn('mother_name');
			$table->dropColumn('special_need');
			$table->dropColumn('uid');
			$table->dropColumn('ration_card_number');
			$table->dropColumn('bpl_proof');
			$table->dropColumn('driving_license_number');
			$table->dropColumn('trainee_contact_details');
			$table->dropColumn('district');
			$table->dropColumn('taluka');

			// Bank Account
			$table->dropColumn('account_name');
			$table->dropColumn('account_number');			
			$table->dropColumn('account_type');
			$table->dropColumn('bank_name');
			$table->dropColumn('bank_branch');
			$table->dropColumn('bank_address');
			$table->dropColumn('bank_ifsc');
			$table->dropColumn('passbook_received');
			
			// On Job Training Details
			$table->dropColumn('ojt_start');
			$table->dropColumn('ojt_end');
			$table->dropColumn('ojt_place');
			$table->dropColumn('ojt_place_address');
			$table->dropColumn('ojt_person_name');
			$table->dropColumn('ojt_contact_number');
			$table->dropColumn('ojt_staypend');

			// Placement Details
			$table->dropColumn('job_start_date');
			$table->dropColumn('job_company');
			$table->dropColumn('job_contact_person');
			$table->dropColumn('job_contact_person_number');
		});
	}

}
