<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRIdToRollNumber extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->dropColumn('r_id');
		});

		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->integer('roll_number')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->dropColumn('roll_number');
		});

		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->integer('r_id')->unsigned()->nullable();
		});
	}

}
