<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Create the 'students' table

		Schema::create('students',function($table)
		{
			$table->increments('id');
			$table->string('r_id')->unique();
			$table->string('name');
			$table->string('education');
			$table->boolean('gender');
			$table->string('category');
			$table->integer('course_id');
			$table->integer('centre_id');	
			$table->datetime('joined_at');
			$table->text('remark');
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Delete the 'students' table
		Schema::drop('students');
	}

}

