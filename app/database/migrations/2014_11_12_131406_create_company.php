<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompany extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies',function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->text('address');
			$table->text('remark')->nullable;
			$table->datetime('created_at')->nullable();
			$table->datetime('updated_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
