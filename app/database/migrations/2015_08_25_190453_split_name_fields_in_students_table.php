<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SplitNameFieldsInStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->dropColumn('name');
		});

		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->string('first_name');
			$table->string('middle_name');
			$table->string('last_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->dropColumn('first_name');
			$table->dropColumn('middle_name');
			$table->dropColumn('last_name');
		});

		Schema::table('students', function(Blueprint $table)
		{
			//
			$table->string('name');
		});
	}

}
