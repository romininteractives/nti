<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CoursesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('courses')->delete();

		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Course::create([
				'name' => $faker->sentence,
				'code' => $faker->unique()->word,
				'description' => $faker->text,
			]);
		}
	}

}