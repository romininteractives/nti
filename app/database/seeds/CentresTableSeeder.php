<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CentresTableSeeder extends Seeder {

	public function run()
	{
		DB::table('centres')->delete();

		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			$city = $faker->city;

			Centre::create([
				'name' => $city,
				'place' => $city,
				'district' => $faker->state,
				'address' => $faker->address,
				'phone_no' => $faker->phoneNumber,
			]);
		}
	}

}