<?php

namespace Nti\Validators;

use Dakshhmehta\LaravelValidation\AbstractValidator;

class SelfHelpGroupValidator extends AbstractValidator{
	public function __construct(array $input = null)
	{
		parent::__construct($input);

		$this->rules = array(
			'shg_group' => 'required',
			'shg_group_member' => 'required',
			'shg_district' => 'required',
			'shg_taluka' => 'required',
			'shg_village' => 'required',
		);
	}
}