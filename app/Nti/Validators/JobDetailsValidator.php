<?php

namespace Nti\Validators;

use Dakshhmehta\LaravelValidation\AbstractValidator;

class JobDetailsValidator extends AbstractValidator {
	public function __construct(array $input = null)
	{
		parent::__construct($input);

		$this->rules = array(
			'job_start_date' => 'date|date_format:Y-m-d|before:'.date('Y-m-d'),
			'job_company' => 'required',
			'job_contact_person' => 'required',
			'job_contact_person_number' => 'numeric|max:12',
			'salary' => 'required|numeric|min:1'
		);
	}
}