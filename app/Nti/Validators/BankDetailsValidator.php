<?php

namespace Nti\Validators;

use Dakshhmehta\LaravelValidation\AbstractValidator;

class BankDetailsValidator extends AbstractValidator {
	public function __construct(array $input = null, $exceptID = null)
	{
		parent::__construct($input);

		$this->rules = array(
			'account_name' => 'required',
			'account_number' => 'required|unique:students',
			'account_type' => 'required|boolean',
			'bank_name' => 'required',
			'bank_branch' => 'required',
			'bank_address' => 'min:5',
			'bank_ifsc' => 'required',
			'passbook_received' => 'required|boolean',
		);

		if(is_numeric($exceptID))
			$this->rules['account_number'] .= ',account_number,'.$exceptID;
	}
}