<?php

namespace Nti\Validators;

use Dakshhmehta\LaravelValidation\AbstractValidator;

class idProofsValidator extends AbstractValidator {
	public function __construct(array $input = null)
	{
		parent::__construct($input);

		$this->rules = array(
			'epic_no' => 'required',
			'uid' => 'required|digits:12',
			'ration_card_number' => 'required',
		);

	}
}