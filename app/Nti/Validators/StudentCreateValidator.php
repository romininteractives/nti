<?php

namespace Nti\Validators;

use Dakshhmehta\LaravelValidation\AbstractValidator;

class StudentCreateValidator extends AbstractValidator {
	public function __construct(array $input = null, $exceptID = null)
	{
		parent::__construct($input);

		$this->rules = array(
			// Personal
			'first_name' => 'required',
			'middle_name' => 'required',
			'last_name' => 'required',
			'mother_name' => 'required',
			'gender' => 'required|boolean',
			'dob'	=>	'required|date|date_format:Y-m-d|before:01-01-2000',
			//'category' => '',
			'address' => 'required',
			'taluka' => 'required',
			'district' => 'required',
			'mobile_number' => 'required|max:10',
			'email' => 'required|email',
			'phone_no' => 'max:12', // Landline number
			//'special_need' => ''
			
			// Academic
			//'education' => '',
			'roll_number' => 'required|unique:students',
			'centre'	=> 'required|exists:centres,id',
			'course'	=> 'required|exists:courses,id',
			'batch_no'	=>	'required',
			'joined_at'	=>	'required|date_format:Y-m-d|before:'.date('Y-m-d'),
		);

		if(is_numeric($exceptID))
			$this->rules['roll_number'] .= ',roll_number,'.$exceptID;
	}
}