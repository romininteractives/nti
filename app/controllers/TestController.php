<?php

use Carbon\Carbon;

class TestController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('filter');
	}

	public function getFilter()
	{
		// Remove all session data
		Session::forget('filter_data');

		return View::make('filter');
	}	

	public function postFilter()
	{
		$input=Input::all();
		

		//dd(Input::get('gender'));
		//`joined_at` like '1993%'
		//'like', '%'.Input::get('q').'%'

		$data = Student::where('category', '=',Input::get('category'))
						->Where('course_id','=',Input::get('course'))
						->Where('centre_id','=',Input::get('centre'))
						->where('district' , '=', Input::get('district'))
						->Where('batch_no','=',Input::get('batch_no'))
						->Where('gender','=',Input::get('gender'))
						->where('first_name', 'like', '%'.Input::get('first_name').'%')
						->where('last_name', 'like', '%'.Input::get('last_name').'%')
						->get();
		
		

		$data = Student::with('course', 'centre');
		// If filter data is not set -> first request
		if(! Session::has('filter_data')) {
			// We set the session data for filter form
			$input = Input::all(); 
			Session::put('filter_data', $input);
		}
		else {
			// The use of form was done in past, we have to use old data
			$input = Session::get('filter_data');
		}

		if(isset($input['category']) && $input['category'] != null)
		{			
			$data=$data->where('category', '=',$input['category']);	
		}
		if(isset($input['course']) && $input['course'] != null)
		{			
			$data=$data->where('course_id','=',$input['course']);	
		}
		if(isset($input['centre']) && $input['centre'] != null)
		{
			$data=$data->where('centre_id','=',$input['centre']);	
		}
		if(isset($input['gender']) && $input['gender'] != null){
			if($input['gender'] == 1)
			{			
				$data=$data->where('gender','=',$input['gender']);	
			}
			else if($input['gender']== 0)
			{
				$data=$data->where('gender','=',$input['gender']);		
			}
		}
		if(Input::get('year') && $input['year'] != null)
		{
			$data->where(function($q){
				foreach(Input::get('year') as $year)
					$data=$q->OrWhere('joined_at','like',$year.'%');	
			});

			$data->orderBy('joined_at', 'DESC');
		}

		//dd($data);
		
		$data = $data->paginate(25);

		return View::make('filterData')->with('msg',$data);
	}

	public function getSingleView($id)
	{
		try {
			$student = Student::findOrFail($id);
			return View::make('viewStudents')->with('student', $student);
			dd($student);
		}
		catch(\Exception $e){
			Debugbar::addException($e);

			return Redirect::back()->with('error', 'Student not found');
		}
	}

}