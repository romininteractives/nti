<?php namespace Controllers\Admin\Centres;

use AdminController;
use Validator;
use Redirect;
use Input;
use Centre;
use View;
use data;
use msg;

class CentresController extends AdminController {

	/**
	 * Show a list of all the blog posts.
	 *
	 * @return View
	 */
	public function getView()
	{
		// Grab all the blog posts
		$centre= Centre::all();

		// Show the page
		return View::make('backend/centres/view_centre')->with('values',$centre);
	}
	public function getCreate()
	{
		// Show the page
		return View::make('backend/centres/create_centre');
	}
	public function postCreate()
	{
		// Show the page
		$rules = array(
			'name'   => 'required',
			'place' => 'required',
			'district'=>'required',
			'address'=>'required',
			'phone_no'=>'required|max:10'
		);
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}
		else
		{
			//insert Value....
			$centre=new Centre;
			$centre->name=Input::get('name');
			$centre->place=Input::get('place');
			$centre->district=Input::get('district');
			$centre->address=Input::get('address');
			$centre->phone_no=Input::get('phone_no');

			$centre->save();
			return Redirect::to('admin/centres');
		}
		
	}
	public function getUpdate($id)
	{
		// Show the page
		//dd ($id);
		$centres=Centre::find($id);
		return View::make('backend/centres/update_centre')->with('data',$centres);
	}
	public function postUpdate($id)
	{
		// Show the page
		$rules = array(
			'name'   => 'required',
			'place' => 'required',
			'district'=>'required',
			'address'=>'required',
			'phone_no'=>'required|max:10'
		);
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}
		else
		{
			//insert Value....
			$centre=Centre::find($id);
			$centre->name=Input::get('name');
			$centre->place=Input::get('place');
			$centre->district=Input::get('district');
			$centre->address=Input::get('address');
			$centre->phone_no=Input::get('phone_no');

			$centre->save();
			return Redirect::to('admin/centres');
		
		}
	}
	public function postDelete()
	{
		$centre=Input::get('delete');

		foreach($centre as $id)
		{
			 //dd($id);
			 $row=Centre::find($id);
			 $row->delete();			
		}

		return Redirect::back();
	}
}
?>