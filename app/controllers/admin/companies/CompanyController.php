<?php namespace Controllers\Admin\Companies;

use AdminController;
use Validator;
use Redirect;
use Input;
use Company;
use View;
use data;
use msg;

class CompanyController extends AdminController {

	public function getView()
	{
		$company= Company::all();

		// Show the page
		return View::make('backend/company/view_company')->with('values',$company);

	}
		public function getCreate()
	{
		return View::make('backend/company/create_company');

	}
		public function postCreate()
	{
		// Show the page
		$rules = array(
			'name'   => 'required'

		);
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}
		else
		{
			//insert Value....
			$company=new Company();
			$company->name=Input::get('name');
			$company->address=Input::get('address');
			$company->remark=Input::get('remark');
			$company->save();
			//return Redirect::to('backend/company/create_company');
			return Redirect::to('admin/companies');
		}
		
	}
	
		public function getUpdate($id)
			{
			// Show the page
			//dd ($id);
				$company=Company::find($id);
				return View::make('backend/company/update_company')->with('data',$company);
			}
		public function postUpdate($id)
			{
		// Show the page
				$rules = array(
				'name'   => 'required'
				);
					$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
					if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}
		else
		{
			//insert Value....
			$company=Company::find($id);
			$company->name=Input::get('name');
			$company->address=Input::get('address');
			$company->remark=Input::get('remark');

			$company->save();
			return Redirect::to('admin/companies');
		
		}
	}
	public function postDelete()
	{
		$company=Input::get('delete');

		foreach($company as $id)
		{
			 //dd($id);
			 $row=Company::find($id);
			 $row->delete();			
		}
		return Redirect::back();
	}
			
}