<?php 

namespace Controllers\Admin\Students;

use AdminController;
use Dakshhmehta\LaravelValidation\Exceptions\ValidationException;
use Nti\Validators\SelfHelpGroupValidator;
use View;
use Input;
use Student;
use Redirect;
use Log;

class SelfHelpGroupController extends AdminController {

	public Function viewForm($studentId){

	try {
			$student = Student::findOrFail($studentId);

			return View::make('backend.students.selfhelpgroup', compact('student'));
		}
		catch(\Exception $e){
			return Redirect::route('students')->with('error', 'Student not found.');
		}

	}
	public Function Update($studentId){

		$input = Input::all();
		$Validators = new SelfHelpGroupValidator($input);

		try {
			
			$Validators->isValid();

			$student = Student::findOrFail($studentId);

			$student->shg_group    		= Input::get('shg_group');
			$student->shg_group_member  = Input::get('shg_group_member');
			$student->shg_district      = Input::get('shg_district');
			$student->shg_village        = Input::get('shg_village');
			$student->shg_taluka		= Input::get('shg_taluka');

			$student->save();

			return Redirect::route('students')->withSuccess('Student Group Added Successfully');
			
		}
		catch(ValidationException $e){
			return Redirect::back()->withErrors($e->getErrors())->withInput();
		}
		 catch (Exception $e) {
				Log::error($e);
			return Redirect::route('students')->with('error', 'Student not found.');
		}
	}
}