<?php

namespace Controllers\Admin\Students;

use AdminController;
use Dakshhmehta\LaravelValidation\Exceptions\ValidationException;
use Input;
use Log;
use Nti\Validators\BankDetailsValidator;
use Redirect;
use Student;
use View;

class UpdateBankDetailsController extends AdminController {
	public function viewForm($studentId){
		try {
			$student = Student::findOrFail($studentId);

			return View::make('backend.students.update.bank_details', compact('student'));
		}
		catch(\Exception $e){
			return Redirect::route('students')->with('error', 'Student not found.');
		}
	}

	public function update($studentId){
		$input = Input::all();
		$validation = new BankDetailsValidator($input, $studentId);

		try {
			$validation->isValid();

			$student = Student::findOrFail($studentId);

			$student->account_name = Input::get('account_name');
			$student->account_number = Input::get('account_number');
			$student->account_type = Input::get('account_type');
			$student->bank_name = Input::get('bank_name');
			$student->bank_branch = Input::get('bank_branch');
			$student->bank_address = Input::get('bank_address');
			$student->bank_ifsc = Input::get('bank_ifsc');
			$student->passbook_received = Input::get('passbook_received');

			$student->save();

			return Redirect::route('students')->withSuccess('Student has been enrolled successfully.');
		}
		catch(ValidationException $e){
			return Redirect::back()->withErrors($e->getErrors())->withInput();
		}
		catch(\Exception $e){
			Log::error($e);
			return Redirect::route('students')->with('error', 'Student not found.');
		}
	}
}