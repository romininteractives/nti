<?php

namespace Controllers\Admin\Students;

use AdminController;
use Dakshhmehta\LaravelValidation\Exceptions\ValidationException;
use Nti\Validators\StudentCreateValidator;
use Nti\Validators\idProofsValidator;
use View;
use Input;
use Student;
use Redirect;
use Log;

class IdProofsController extends AdminController {

	public function viewForm($studentId){
		try {
			$student = Student::findOrFail($studentId);

			return View::make('backend.students.idproof', compact('student'));
		}
		catch(\Exception $e){
			return Redirect::route('students')->with('error', 'Student not found.');
		}
	}

	public function update($studentId){

		$input = Input::all();
		$validation = new idProofsValidator($input);

		try {

			$validation->isValid();

			$student = Student::findOrFail($studentId);

			$student->epic_no			 	= Input::get('epic_no');
			$student->uid 				 	= Input::get('uid');
			$student->ration_card_number 	= Input::get('ration_card_number');
			$student->bpl_proof 		    = Input::get('bpl_proof');
			$student->driving_license_number = Input::get('driving_license_number');

			$student->save();

			return Redirect::route('students')->withSuccess('Student ID Proofs Added successfully');
		}
		catch(ValidationException $e){
			return Redirect::back()->withErrors($e->getErrors())->withInput();
		}
		catch(\Exception $e){
			Log::error($e);
			return Redirect::route('students')->with('error', 'Student not found.');
		}
	}
}