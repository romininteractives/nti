<?php

namespace Controllers\Admin\Students;

use AdminController;
use Dakshhmehta\LaravelValidation\Exceptions\ValidationException;
use Nti\Validators\StudentCreateValidator;
use View;
use Input;
use Student;
use Redirect;

class CreateStudentController extends AdminController {
	public function viewForm(){
		return View::make('backend.students.create');
	}

	public function addStudent(){
		$input = Input::all();
		$validation = new StudentCreateValidator($input);

		try {
			$validation->isValid();

			$student = new Student;

			$student->first_name = Input::get('first_name');
			$student->middle_name = Input::get('middle_name');
			$student->last_name = Input::get('last_name');
			$student->mother_name = Input::get('mother_name');
			$student->gender = Input::get('gender');
			$student->dob = Input::get('dob');
			$student->category = Input::get('category', 'general');
			$student->address = Input::get('address');
			$student->taluka = Input::get('taluka');
			$student->district = Input::get('district');
			$student->mobile_number = Input::get('mobile_number');
			$student->email = Input::get('email');
			$student->phone_no = Input::get('phone_no');
			$student->special_need = Input::get('special_need');

			$student->education = Input::get('education', '');
			$student->roll_number = Input::get('roll_number');
			$student->centre_id = Input::get('centre');
			$student->course_id = Input::get('course');
			$student->batch_no = Input::get('batch_no');
			$student->joined_at = Input::get('joined_at');

			$student->save();

			return Redirect::route('students')->withSuccess('Student has been enrolled successfully.');
		}
		catch(ValidationException $e){
			return Redirect::back()->withErrors($e->getErrors())->withInput();
		}
	}

	public function getUpdate($studentId){

		try {
			$student = Student::findOrFail($studentId);

			return View::make('backend.students.updateStudents', compact('student'));
		}
		catch(\Exception $e){
			return Redirect::route('students')->with('error', 'Student not found.');
		}
	}
	
	public function postUpdate($studentId){

		$input = Input::all();
		$validation = new StudentCreateValidator($input , $studentId);

		try {

			$validation->isValid();

			$student = Student::findOrFail($studentId);

			$student->first_name = Input::get('first_name');
			$student->middle_name = Input::get('middle_name');
			$student->last_name = Input::get('last_name');
			$student->mother_name = Input::get('mother_name');
			$student->gender = Input::get('gender');
			$student->dob = Input::get('dob');
			$student->category = Input::get('category', 'general');
			$student->address = Input::get('address');
			$student->taluka = Input::get('taluka');
			$student->district = Input::get('district');
			$student->mobile_number = Input::get('mobile_number');
			$student->email = Input::get('email');
			$student->phone_no = Input::get('phone_no');
			$student->special_need = Input::get('special_need');

			$student->education = Input::get('education', '');
			$student->roll_number = Input::get('roll_number');
			$student->centre_id = Input::get('centre');
			$student->course_id = Input::get('course');
			$student->batch_no = Input::get('batch_no');
			$student->joined_at = Input::get('joined_at');

			$student->save();

			return Redirect::route('students')->withSuccess('Student has been Updated Successfully.');

			
		} catch(ValidationException $e){
			return Redirect::back()->withErrors($e->getErrors())->withInput();
		}
		catch(\Exception $e){
			Log::error($e);
			return Redirect::route('students')->with('error', 'Student not found.');
		}

	}
}