<?php namespace Controllers\Admin\Courses;

use Course;
use Validator;
use Input;
use Redirect;

class CoursesController extends \AdminController {
	protected $rules = array(
		'name' => 'required',
		'code' => 'required|unique:courses,code',
	);

	public function __construct(){
		parent::__construct();

		$this->baseView .= '/courses';	
	}

	public function getIndex(){
		$courses = Course::all();

		return $this->view('index')->with('courses', $courses);
	}

	public function getCreate(){
		return $this->view('create');
	}

	public function postCreate(){
		$validation = Validator::make(Input::all(), $this->rules);

		if($validation->passes()){
			$course = new Course;

			$course->name = Input::get('name');
			$course->code = Input::get('code');
			$course->description = Input::get('description');

			$course->save();

			return Redirect::route('courses')->with('success', 'Course has been added');
		}
		else {
			return Redirect::back()->withInput()->withErrors($validation);
		}
	}

	public function getUpdate($id){
		try {
			$course = Course::findOrFail($id);

			return $this->view('update')->withCourse($course);
		}
		catch(\Exception $e){
			return Redirect::route('courses')->with('error', 'Invalid course');
		}
	}

	public function postUpdate($id){
		try {
			$this->rules['code'] .= ','.$id; // Except current record

			$validation = Validator::make(Input::all(), $this->rules);

			if($validation->passes()){
				$course = Course::findOrFail($id);

				$course->name = Input::get('name');
				$course->code = Input::get('code');
				$course->description = Input::get('description');

				$course->save();

				return Redirect::route('courses')->with('success', 'Course has been updated');
			}
			else {
				return Redirect::back()->withInput()->withErrors($validation);
			}

		}
		catch(\Exception $e){
			return Redirect::route('courses')->with('error', 'Invalid course');
		}
	}

	public function getDelete($id){
		try {
			$course = Course::findOrFail($id);

			$course->delete();

			return Redirect::route('courses')->with('success', 'Course has been deleted');
		}
		catch(\Exception $e){
			return Redirect::route('courses')->with('error', 'Invalid course');
		}
	}
}