<?php 

$faker = Faker\Factory::create();

$I = new AcceptanceTester($scenario);
$I->wantTo('enroll a new student');

$category = ['GENERAL', 'OBC', 'A', 'E', 'MO'];

$I->amOnPage('/admin/students/add');
$I->login();
$I->seeInTitle('Enroll');

$I->fillField('first_name', $faker->firstName);
$I->fillField('middle_name', $faker->firstNameMale);
$I->fillField('last_name', $faker->lastName);
$I->fillField('mother_name', $faker->name('female'));
$I->fillField('gender', rand(0,1));
$I->fillField('dob', $faker->dateTimeThisCentury->format('Y-m-d'));
$I->fillField('email', $faker->freeEmail);
$I->fillField('address', $faker->address);
$I->fillField('taluka', $faker->city);
$I->fillField('district', $faker->state);
$I->fillField('mobile_number', rand(1000000000,9999999999));
$I->fillField('phone_no', rand(1000000000,9999999999));
$I->fillField('category', $category[rand(0, count($category)-1)]);
$I->fillField('special_need', $faker->word);

$I->click('Acadamic');

$I->fillField('education', $faker->word);
$I->fillField('roll_number', $faker->randomNumber);

$I->click('Submit');
$I->see('Student has been enrolled successfully.');